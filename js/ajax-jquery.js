function sumar()
{
    // Obtener datos
    var numero1 = $('#numero1').val();
    var numero2 = $('#numero2').val();
    var cmd ="sumarDatos";

    $.ajax({
        method:'post',
        url:'php/command.php',
        data:{
            'numero1':numero1,
            'numero2':numero2,
            'cmd':cmd
        }
    })
    .done(function(suma){
        $('#mensaje').html('Resultado :'+suma);
    })
    .fail(function(excepcion){
        alert('Error: '+excepcion);
    })

}