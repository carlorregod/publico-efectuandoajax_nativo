function sumar()
{
    // Obtener datos
    var numero1 = document.getElementById('numero1').value;
    var numero2 = document.getElementById('numero2').value;

    // Armar parámetros
    var params  = "&cmd=sumarDatos"
                + '&numero1=' + numero1
                +'&numero2=' + numero2;

    // AJAX
    var xhttp = window.XMLHttpRequest ? new XMLHttpRequest(): new ActiveXObject("Microsoft.XMLHTTP");  //XMLHttpRequest inicio de pagina
    xhttp.onreadystatechange = function() 
    {
        if (this.readyState == 4 && this.status == 200) 
            document.getElementById('mensaje').innerHTML = 'Resultado: '+this.responseText; // El servidor procesa la solicitud.
    };

    xhttp.open("POST", "php/command.php", false);    // Tipo de comunicacion true (asínchrono) or false (síncrono)
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(params);  // Envío de http

    // GET
    // xhttp.open("GET", "command.php"+params, true);
    // xhttp.send();


    // ajax.send('cmd=sumarDatos' + params, function () {
    //     if (this.status == 200 && this.readyState == 4) {     // El servidor procesa la solicitud.
    //         var respuesta = this.responseText

    //         document.getElementById('mensaje').innerHTML = 'Resultado: '+respuesta;
    //     }
    // });
}

//AJAX

//--TIPS
// *Lee los datos de un servidor web - después de que la página se haya cargado
// *Actualizar una página web sin volver a cargar la página.
// *Enviar datos a un servidor web

// Que es Ajax?

// AJAX = Asíncrono Javascript And XML.
// AJAX no es un lenguaje de programación.
/* AJAX permite que las páginas web se actualicen de forma asíncrona al
   intercambiar datos con un servidor web previamente.
   Esto significa que es posible actualizar partes de una página web,
   sin volver a cargar toda la página.*/