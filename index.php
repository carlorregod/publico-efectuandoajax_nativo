<!doctype html>
<html lang="es">
<head>
    <title>Ejemplo AJAX Nativo</title>
</head>

<body>
    <div>
        <h3 style="text-align: center;">Prueba Calculadora</h3>
        <div>
            <label for="nombre">Número 1:</label>
            <input type="text" style="width:100px;" id="numero1" name="numero1">
        </div>
        <div>
            <br>
            <label for="nombre">Número 2:</label>
            <input type="text" style="width:100px;" id="numero2" name="numero2">
        </div>
            <br>
            <button type="button" onclick="sumar();">Sumar</button>
        </div>
        </div>
            <br>
            <span id="mensaje"></span>
        </div>
    </div>
    <script type="text/javascript" src="js/ajax.js"></script>
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/ajax-jquery.js"></script>-->
</body>
</html>